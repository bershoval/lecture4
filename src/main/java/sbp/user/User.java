package sbp.user;

public class User {

    private String id;
    private String firstName;
    private String lastName;
    private int age;
    private String phone;

    public User(String id, String firstName, String lastName, int age, String phone) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public String toString() {
        return  "id: " + id + "FirstName: " + firstName + ", LastName: " + firstName + ", Age: " + age + ", Phone: " + phone;
    }


}
