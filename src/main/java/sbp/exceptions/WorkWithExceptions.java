package sbp.exceptions;

public class WorkWithExceptions
{
    /**
     * В данном методе необходимо вызвать методы throwCheckedException и throwUncheckedException.
     * Все исключения должны быть обработаны
     * Необходимо вывести описание exception и stacktrace в консоль
     * Впойманное исключение необходимо упаковать в кастомное исключение и пробросить выше
     * Перед завершением работы метода обязательно необходимо вывести в консоль сообщение "Finish"
     */
    public void exceptionProcessing() throws CustomException

    {
        try {

            throwCheckedException();
            throwUncheckedException();
        }

        catch (RuntimeException e){

            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new CustomException("throw custom exception");

        }
        catch (Exception e){

            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new CustomException("throw custom exception");

        }

        finally {
            System.out.println("Finish");
        }

    }

    /**
     * (* - необязательно) Доп. задание.
     * Выписать в javadoc (здесь) - все варианты оптимизации и устранения недочётов метода
     * @throws IllegalStateException
     * @throws Exception
     * @throws RuntimeException
     */
    public void hardExceptionProcessing() throws RuntimeException
    {
        System.out.println("Start");
        try
        {
            System.out.println("Step 1");
            throw new IllegalArgumentException();
        }
        catch (IllegalArgumentException e)
        {
            e.printStackTrace();
            System.out.println("Catch IllegalArgumentException" + e.getMessage());
            throw new RuntimeException("Step 2", e);
        }

        finally
        {
            System.out.println("Step finally");
        }
    }

    private void throwCheckedException() throws Exception
    {
        throw new Exception("Checked exception");
    }

    private void throwUncheckedException()
    {
        throw new RuntimeException("Unchecked exception");
    }
}
