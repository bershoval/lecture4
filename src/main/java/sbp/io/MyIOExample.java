package sbp.io;

import java.io.*;
import java.util.Date;

public class MyIOExample
{
    /**
     * Создать объект класса {@link java.io.File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     *      - размер
     *      - время последнего изменения
     * Необходимо использовать класс {@link java.io.File}
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */

    public boolean workWithFile(String fileName)
    {
        File file = new File(fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean result = file.isFile();

        if (result) {
            System.out.println("сущность является файлом: " + result);
            System.out.println("абсолютный путь к файлу: " + file.getAbsolutePath());
            System.out.println("родительский путь к файлу: " + file.getParent());
            System.out.println("размер файла " + file.length() + " байт");
            Long lastModified = file.lastModified();
            Date date = new Date(lastModified);
            System.out.println("время последнего изменения: " + date);

        }
        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileInputStream} и {@link java.io.FileOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName)
    {
        File sourceFile = new File(sourceFileName);
        File destinationFile = new File(destinationFileName);

        try
        {
            FileInputStream fileInputStream = new FileInputStream(sourceFile);
            FileOutputStream fileOutputStream = new FileOutputStream(destinationFile);

            int bufferSize;
            byte[] bufffer = new byte[1024];
            while ((bufferSize = fileInputStream.read(bufffer)) > 0)
                fileOutputStream.write(bufffer, 0, bufferSize);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.BufferedInputStream} и {@link java.io.BufferedOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName)
    {
        File sourceFile = new File(sourceFileName);
        File destinationFile = new File(destinationFileName);

        try
        {
            FileInputStream fileInputStream = new FileInputStream(sourceFile);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);

            FileOutputStream fileOutputStream = new FileOutputStream(destinationFile);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);

            int bufferSize;
            byte[] bufffer = new byte[1024];
            while ((bufferSize = fileInputStream.read(bufffer)) > 0)
                fileOutputStream.write(bufffer, 0, bufferSize);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        return true;

    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileReader} и {@link java.io.FileWriter}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName)
    {
        File sourceFile = new File(sourceFileName);
        File destinationFile = new File(destinationFileName);


        try { FileReader fileReader = new FileReader(sourceFile);
            FileWriter fileWriter = new FileWriter(destinationFile);

            char [] buffer = new  char [1024];
            int count = 0;
            int n = 0;
            while (-1 != (n = fileReader.read (buffer))){
                fileWriter.write(buffer, 0, n);
                count += n;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
