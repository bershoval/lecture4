package sbp.io;

import java.io.*;
import java.nio.file.*;
import java.util.Date;

public class MyNIO2Example {
    /**
     * Создать объект класса {@link java.io.File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     * - абсолютный путь
     * - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     * - размер
     * - время последнего изменения
     * Необходимо использовать класс {@link java.io.File}
     *
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName) {

        Path path = Paths.get(fileName);
        boolean resultExistFile = Files.exists(path, LinkOption.NOFOLLOW_LINKS);

        System.out.println("ExistFile: " + resultExistFile);
        System.out.println("AbsolutePath: " + path.toAbsolutePath());
        System.out.println("ParentsPath: " + path.getRoot());

        try {
            System.out.println("path size: " + Files.size(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        File fileIO = path.toFile();
        Long lastModified = fileIO.lastModified();
        Date date = new Date(lastModified);
        System.out.println("время последнего изменения: " + date);

        return true;
    }


    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileInputStream} и {@link java.io.FileOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName) {

        try {
            Path sourcePath = Paths.get(sourceFileName);
            Path destinationPath = Paths.get(destinationFileName);
            Files.copy(sourcePath, destinationPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("error");
            return false;
        }

        return true;
    }


    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.BufferedInputStream} и {@link java.io.BufferedOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName) {

        try {
            Path sourcePath = Paths.get(sourceFileName);
            Path destinationPath = Paths.get(destinationFileName);

            BufferedReader bufferedReader = Files.newBufferedReader(sourcePath);
            InputStream inputStream = Files.newInputStream(sourcePath);

            BufferedWriter bufferedWriter = Files.newBufferedWriter(destinationPath);
            OutputStream outputStream = Files.newOutputStream(destinationPath);

            int bufferSize;
            byte[] bufffer = new byte[1024];
            while ((bufferSize = inputStream.read(bufffer)) > 0)
                outputStream.write(bufffer, 0, bufferSize);

        } catch (IOException e) {

            e.printStackTrace();
            return false;
        }

        return true;
    }


    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileReader} и {@link java.io.FileWriter}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName)  {

        Path sourcePath = Paths.get(sourceFileName);
        Path destinationPath = Paths.get(destinationFileName);


        InputStream inputStream = null;
        OutputStream outputStream = null;

        try {
            inputStream = Files.newInputStream(sourcePath);
            outputStream = Files.newOutputStream(destinationPath);

            int i;
            do {
                i = inputStream.read();
                if (i != -1) outputStream.write(i);
            } while (i != -1);

        } catch(IOException e) {
            System.out.println("Ошибка ввода-вывода: " + e);
            return false;
        } finally {

            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                System.out.println("Ошибка при закрытии входного файла");
            }
            try {
                if (outputStream != null) outputStream.close();
            } catch (IOException e) {
                System.out.println("Ошибка при закрытии выходного файла");
            }
        }
        return true;
    }
}
