package sbp.cmp;

import java.util.Comparator;
import java.util.Objects;

public class Person implements Comparable <Person> {
    private String name;
    private String city;
    private int age;

    public Person(String name,String city, int age) {

        if (name == null || city == null) {
            throw new IllegalArgumentException("arguments is null");
        }
        else
        {
            this.name = name;
            this.age = age;
            this.city = city;
        }
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if(this == obj){
            return true;
        }
        if(!this.getClass().equals(obj.getClass())){
            return false;
        }

        Person other = (Person) obj;
        return city.equalsIgnoreCase(other.city) && name.equalsIgnoreCase(other.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city,name);
    }

    @Override
    public int compareTo(Person other) {

        return this.city.compareToIgnoreCase(other.city);
    }

    public static Comparator<Person> nameComparator = new Comparator<Person>() {
        @Override
        public int compare(Person lhs, Person rhs) {
            return lhs.getName().compareTo(rhs.getName());
        }
    };

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                '}';
    }
}


