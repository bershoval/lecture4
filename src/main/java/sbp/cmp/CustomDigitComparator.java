package sbp.cmp;

import java.util.Comparator;
import java.util.Objects;

public class CustomDigitComparator implements Comparator <Integer> {
    @Override
    public int compare(Integer lhs, Integer rhs) {

        if (Objects.isNull(lhs) || Objects.isNull(rhs)) {

            throw new IllegalArgumentException("arguments is null");
        }
        else
        {
            if (lhs % 2 == 0) {
                if (rhs % 2 == 0) {
                    return lhs.compareTo(rhs);
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if (rhs % 2 == 0) {
                    return 1;
                }
                else
                {
                    return lhs.compareTo(rhs);
                }
            }
        }
    }
}

