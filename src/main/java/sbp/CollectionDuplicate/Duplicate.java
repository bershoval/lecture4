package sbp.CollectionDuplicate;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Duplicate <T> {

    public List<T> duplicates_firstMethod(List<T> items) {

        List<T> newList = items.stream()
                .filter(e -> Collections.frequency(items, e) > 1)
                .distinct()
                .collect(Collectors.toList());

        return newList;
    }

    public List<T> duplicates_secondMethod(List<T> items) {

        List<T> newList = items.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .filter(e -> e.getValue() > 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        return newList;
    }

    public List<T> duplicates_thirdMethod(List<T> items) {

        List<T> nonDistinctElements = new ArrayList<>();
        for (T s : items)
            if (items.indexOf(s) != items.lastIndexOf(s))
                if (!nonDistinctElements.contains(s))
                    nonDistinctElements.add(s);

        return nonDistinctElements;
    }
}
