package sbp.collections;


import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

public class CustomArrayImpl <T> implements CustomArray <T> {

    /**
     * Емкость коллекции по умолчанию
     */
    private static final int DEFAULT_CAPACITY = 10;

    private T[] myArray;
    private int capacity;
    private int size;


    public CustomArrayImpl() {

        this.myArray = (T[]) new Object[DEFAULT_CAPACITY];
        this.capacity = DEFAULT_CAPACITY;
        this.size = 0;
    }

    public CustomArrayImpl(int capacity) {
        this.myArray = (T[]) new Object[capacity];
        this.capacity = capacity;
        this.size = 0;
    }

    public CustomArrayImpl(Collection<T> c) {
        myArray = (T[]) c.toArray();
        this.capacity = DEFAULT_CAPACITY;
        size = myArray.length;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean add(T item) {
        if(Objects.isNull(item)){
            return false;
        }

        if (size + 1 <= capacity) {
            this.myArray[size] =  item;
            size++;
            return true;
        }
        else
        {
            final T[] tempArray = this.myArray;
            this.myArray = Arrays.copyOf(tempArray,capacity + DEFAULT_CAPACITY);
            this.capacity += DEFAULT_CAPACITY;
            this.myArray[size] = item;
            size++;
            return true;
        }
    }

    @Override
    public boolean addAll(T[] items) {
        int newSize = size + items.length;

        if (newSize <= capacity) {
            int j = 0;
            for(int i = size; i < newSize; i++, j++){
                myArray[i] = items[j];
            }
            size = newSize;
            return true;
        }
        else
        {
            final T[] tempArray = this.myArray;
            this.myArray = Arrays.copyOf(tempArray,capacity + DEFAULT_CAPACITY);
            this.capacity += DEFAULT_CAPACITY;
            int j = 0;
            for(int i = size; i < newSize; i++, j++){
                myArray[i] = items[j];
            }
            size = newSize;
            return true;
        }
    }

    @Override
    public boolean addAll(Collection <T> items) {
        Object[] newArray =  items.toArray();
        int newSize = size + newArray.length;

        if(newSize <= capacity){
            int j = 0;
            for(int i = size; i < newSize; i++, j++){
                myArray[i] = (T)newArray[j];
            }
            size = newSize;
            return true;
        }
        else
        {
            final T[] tempArray = this.myArray;
            this.myArray = Arrays.copyOf(tempArray,capacity + DEFAULT_CAPACITY);
            this.capacity += DEFAULT_CAPACITY;
            int j = 0;
            for(int i = size; i < newSize; i++, j++){
                myArray[i] = (T)newArray[j];
            }
            size = newSize;
            return true;
        }
    }

    @Override
    public boolean addAll(int index, Object[] items) {
        int newSize = size + items.length;
        Object[] newMyArray = (T[]) new Object[newSize];

        if (newSize <= capacity) {

            for (int i = 0; i < index; i++) {
                newMyArray[i] = myArray[i];
            }
            int addIndexStart = index;
            int addIndexFinished = index + items.length;
            for (int j = 0; addIndexStart < addIndexFinished; addIndexStart++, j++) {
                newMyArray[addIndexStart] = items[j];
            }

            int nextIndex = addIndexFinished;
            int tmpIndex = index;

            for (int i = nextIndex; i < newSize; i++) {
                newMyArray[i] = myArray[tmpIndex++];
            }
            myArray = (T[]) newMyArray;
            size = newSize;
            return true;
        }
        else
        {
            this.capacity += DEFAULT_CAPACITY;
            for (int i = 0; i < index; i++) {
                newMyArray[i] = myArray[i];
            }
            int addIndexStart = index;
            int addIndexFinished = index + items.length;
            for (int j = 0; addIndexStart < addIndexFinished; addIndexStart++, j++) {
                newMyArray[addIndexStart] = items[j];
            }

            int nextIndex = addIndexFinished;
            int tmpIndex = index;

            for (int i = nextIndex; i < newSize; i++) {
                newMyArray[i] = myArray[tmpIndex++];

            }
            myArray = (T[]) newMyArray;
            size = newSize;
            return true;
        }
    }

    @Override
    public T get(int index) {

        return (T) myArray[index];
    }

    @Override
    public T set(int index, T item){
        T oldValue = (T) myArray[index];
        myArray[index] = item;
        return oldValue;
    }

    @Override
    public void remove(int index) {
        final T[] tempArray = (T[]) new Object[size];
        int j = 0;
        for(int i = 0; i < size; i++){
            if( i != index) {
                tempArray[j++] = myArray[i];
            }
        }
        size--;
        myArray = tempArray;
    }

    @Override
    public boolean remove(T item) {

        Object[] newData = new Object[size - 1];
        int n = 0;
        int index = 0;
        boolean result = false;

        if (item == null) {
            for (; index < size; index++) {
                if (myArray[index] == null) {
                    result = true;
                    break;
                }
                if (index == size - 1) {
                    return false;
                }
                newData[n++] = myArray[index];
            }

        } else {
            for (; index < size; index++) {
                if (item.equals(myArray[index])) {
                    result = true;
                    break;
                }
                if (index == size - 1) {
                    return false;
                }
                newData[n++] = myArray[index];
            }

        }
        index++;
        for (; index < size; index++) {
            newData[n++] = myArray[index];
        }

        --size;
        myArray = (T[]) newData;

        return result;
    }

    @Override
    public boolean contains(T t) {
        if(Objects.isNull(t)){
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (myArray[i].equals(t)) {
                return true;
            }
        }
        return false;
    }
    @Override
    public int indexOf(T item) {
        if (item == null) {
            for (int i = 0; i < size; i++)
                if (myArray[i] == null)
                    return i;
        } else {
            for (int i = 0; i < size; i++)
                if (item.equals(myArray[i]))
                    return i;
        }
        return -1;
    }

    @Override
    public void ensureCapacity(int newElementsCount) {

        if (size + newElementsCount <= capacity) {
            return;
        }
        else
        {
            final T[] tempArray = this.myArray;
            this.myArray = Arrays.copyOf(tempArray,capacity + DEFAULT_CAPACITY);
            this.capacity += DEFAULT_CAPACITY;
        }
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public void reverse() {
        Object[] reverseMyArray = new Object[size];
        int j = 0;
        for(int i = size - 1; i >= 0; i--, j++){
            reverseMyArray[j] = myArray[i];
        }
        myArray = (T[]) reverseMyArray;
    }

    @Override
    public Object[] toArray() {
        return this.myArray;
    }

    @Override
    public String toString() {
        return "CustomArrayImpl{" +
                "myArray=" + Arrays.toString(myArray) +
                ", size=" + size +
                '}';
    }
}
