package sbp.db;

import java.sql.*;

public class DBService {

    private final static String URL = "jdbc:sqlite:C:\\Users\\HP Desktop Pro\\Desktop\\sber_school\\db\\database.db";

    private Connection connection;
    private Statement statement;
    private PreparedStatement preparedStatement;

    public Statement buildStatement() throws SQLException {

        this.connection = DriverManager.getConnection(URL);
        return this.connection.createStatement();
    }


    public void close() throws SQLException {
        this.statement.close();
        this.connection.close();
    }

    public boolean executeQuery(String query) throws SQLException {

        try (Connection connection = DriverManager.getConnection(URL);
             Statement statement = connection.createStatement()) {
            return statement.execute(query);
        }
    }
}
