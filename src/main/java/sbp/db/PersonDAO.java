package sbp.db;

import sbp.exceptions.CustomException;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class PersonDAO {

    private DBService db;

    public PersonDAO(DBService dbService)
    {
        this.db = dbService;
    }

    public boolean createTable() {

        try {
            String sql = "create table person (" +
                    " id Integer(32)," +
                    " name varchar(32)," +
                    " age Integer(32)," +
                    " phone varchar(32)" +
                    ");";
            this.db.executeQuery(sql);
            System.out.println("create table completed");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean addPerson(Person person)  {
        try {
            final String createPersonQuery = String.format("insert into person ('id', 'name', 'age', 'phone') VALUES ('%s', '%s', '%s', '%s')",
                    person.getId(), person.getName(), person.getAge(), person.getPhone());
            this.db.executeQuery(createPersonQuery);
            System.out.println("add person completed");
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Collection<Person> readAllPerson() throws CustomException {

        final String readAllPersonQuery = "select * from person";

        try(Statement statement = this.db.buildStatement();
            ResultSet resultSet = statement.executeQuery(readAllPersonQuery);)
        {
            List<Person> personList = new ArrayList<>();
            while(resultSet.next())
            {
                Person localPerson = new Person();
                localPerson.setId(resultSet.getInt("id"));
                localPerson.setName(resultSet.getString("name"));
                localPerson.setAge(resultSet.getInt("age"));
                localPerson.setPhone(resultSet.getString("phone"));
                personList.add(localPerson);
            }
            return personList;
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new CustomException("throw custom exception");
        }
    }

    public Person findById(int id) throws CustomException{

        String findByIdPerson = "select * from person where id = '" + id + "'";
        try(ResultSet resultSet = this.db.buildStatement().executeQuery(findByIdPerson);)
        {

            if (resultSet.next() == false) {
                return null;
            }
            Integer personId = resultSet.getInt("id");
            String name = resultSet.getString("name");
            Integer age = resultSet.getInt("age");
            String phone = resultSet.getString("phone");
            System.out.println("find by id completed");

            return new Person(personId, name, age, phone);
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new CustomException("throw custom exception");
        }
    }

    public boolean deletePerson(int id){

        try{
            final String deleteQuery = "delete from person where id = " + id;
            this.db.executeQuery(deleteQuery);
            System.out.println("delete person id: " + id);
        }
        catch(SQLException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean changeName(int id, String newName){

        try
        {
            final String changePersonName = ("update person set name = '" + newName + "' where id ='" + id + "';");
            this.db.executeQuery(changePersonName);
            System.out.println("change name completed");
        }
        catch (SQLException e)
        {
            System.out.println("Failed to .changeName " + e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean dropTable(){

        try  {
            final String dropTable = "drop table person";
            this.db.executeQuery(dropTable);
            System.out.println("drop table completed");
        }
        catch (SQLException e)
        {
            System.out.println("Failed to .dropTable " + e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }
}




