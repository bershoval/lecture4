package sbp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.cmp.Person;

import java.util.ArrayList;
import java.util.List;

public class PersonTests {

    /**
     * После отработки метода sort(Person::compareTo), жидается сортировка обьектов
     * коллекции myPerson, по полю city.
     * Ожидаем:
     *   - обьект person1 по индексу коллекции 1;
     *   - обьект person2 по индексу коллекции 0;
     *   - обьект person3 по индексу коллекции 2;
     *
     * После отработки метода sort(Person.nameComparator), ожидаеться сортировка обьектов
     * коллекции myPerson, по полю name.
     * Ожидаем:
     *    - обьект person1 по индексу коллекции 1;
     *    - обьект person2 по индексу коллекции 2;
     *    - обьект person3 по индексу коллекции 0;
     */

    @Test
    public void comparablePersonTest(){

        List<Person> myPerson = new ArrayList<>();

        Person person1 = new Person("Bob", "Bor", 25);
        Person person2 = new Person("Denis","Arzamas",22);
        Person person3 = new Person("Aleksey", "Sarov",31);

        myPerson.add(person1);
        myPerson.add(person2);
        myPerson.add(person3);

        myPerson.sort(Person::compareTo);
        Assertions.assertEquals(person1,myPerson.get(1));
        Assertions.assertEquals(person2,myPerson.get(0));
        Assertions.assertEquals(person3,myPerson.get(2));

        myPerson.sort(Person.nameComparator);
        Assertions.assertEquals(person1, myPerson.get(1));
        Assertions.assertEquals(person2, myPerson.get(2));
        Assertions.assertEquals(person3, myPerson.get(0));
    }

    /**
     * При создании обьекта Person(null,"Alex",25) с аггументом name = null, ожидаем
     * подброс исключения IllegalArgumentException.
     */
    @Test
    public void argumentIsNull(){

        Assertions.assertThrows(IllegalArgumentException.class, () -> new Person(null,"Alex",25));

    }
}



