package sbp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.db.DBService;
import sbp.db.Person;
import sbp.db.PersonDAO;
import sbp.exceptions.CustomException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public class PersonDAOTest {

    /**
     * Проверяем успешный сценарий создания таблицы person
     */
    @Test
    public void createTable_Success_Test(){

        PersonDAO dao = new PersonDAO(new DBService());
        boolean result = dao.createTable();
        Assertions.assertTrue(result);
    }

    /**
     * Проверяем неуспешный сценарий создания таблицы person
     */
    @Test
    public void createTable_Fail_Test() throws SQLException {

        DBService dbServiceMock = Mockito.mock(DBService.class);
        Mockito.doThrow(new SQLException("error method .createTable")).when(dbServiceMock).executeQuery(Mockito.anyString());
        PersonDAO dao = new PersonDAO(dbServiceMock);
        boolean result = dao.createTable();
        Assertions.assertFalse(result);
    }

    /**
     * Проверяем успешный сценарий записи нового обьекта {@Person}
     */
    @Test
    public void addPerson_Success_Test(){

        Person personForTest = new Person(1,"Alex", 33,"8-800-1");
        PersonDAO dao = new PersonDAO(new DBService());

        boolean result = dao.addPerson(personForTest);

        Assertions.assertTrue(result);
    }


    /**
     * Проверяем неуспешный сценарий записи нового обьекта {@Person}
     * @throws SQLException
     */
    @Test
    public void addPerson_Fail_Test() throws SQLException {

        Person personForTest = new Person(1,"Alex", 33,"8-800-1");
        DBService dbServiceMock = Mockito.mock(DBService.class);
        Mockito.doThrow(new SQLException("error method .addPerson")).when(dbServiceMock).executeQuery(Mockito.anyString());

        PersonDAO dao = new PersonDAO(dbServiceMock);
        boolean result = dao.addPerson(personForTest);
        Assertions.assertFalse(result);
    }

    /**
     * Проверяем успешний сценарий удаления обьекта {@Person} по номеру id
     */
    @Test
    public void deletePerson_Success_Test(){

        Person person1 = new Person(2,"Eric ",27,"8-800-2");
        Person person2 = new Person(3,"Derek", 28,"8-800-3");

        PersonDAO dao = new PersonDAO(new DBService());
        dao.addPerson(person1);
        dao.addPerson(person2);
        boolean result = dao.deletePerson(2);
        Assertions.assertTrue(result);
    }

    /**
     * Проверяем неуспешний сценарий удаления обьекта {@Person} по номеру id;
     * Для проверки содаем новый DBService, с помощью Mockito, задаем условие постоянного
     * выброса исключения;
     * Ожидаем false после отработки метода deletePerson(2);
     */
    @Test
    public void deletePerson_Fail_Test() throws SQLException {

        Person person1 = new Person(2,"Eric",27,"8-800-2");
        Person person2 = new Person(3,"Derek", 28,"8-800-3");
        PersonDAO dao = new PersonDAO(new DBService());
        dao.addPerson(person1);
        dao.addPerson(person2);

        DBService dbServiceMock = Mockito.mock(DBService.class);
        Mockito.doThrow(new SQLException("error method .deletePerson")).when(dbServiceMock).executeQuery(Mockito.anyString());
        PersonDAO dao1 = new PersonDAO(dbServiceMock);
        boolean result = dao1.deletePerson(2);
        Assertions.assertFalse(result);
    }

    /**
     * Проверяем успешний сценарий изменения имени обьекта {@Person} по номеру id.
     * Заменяем имя "Harry" на имя "Jason", ожидаем true после отработки метода dao.changeName(5,"Jason");
     * С помощью метода dao.findById(5), находим обьект {@Person} под номером id равным 5,
     * ожидаем поле name равное "Jason".
     */
    @Test
    public void changeName_Success_Test() throws CustomException{

        Person person1 = new Person(4,"Calvin",25,"8-800-4");
        Person person2 = new Person(5,"Harry", 27,"8-800-5");
        PersonDAO dao = new PersonDAO(new DBService());
        dao.addPerson(person1);
        dao.addPerson(person2);

        boolean result = dao.changeName(5,"Jason");
        Assertions.assertTrue(result);
        Person tempPerson = dao.findById(5);
        Assertions.assertEquals("Jason", tempPerson.getName());
    }

    /**
     * Проверяем неуспешний сценарий изменения имени обьекта {@Person} по номеру id;
     * @throws SQLException
     */
    @Test
    public void changeName_Fail_Test() throws SQLException {

        Person person1 = new Person(4,"Calvin",25,"8-800-4");
        Person person2 = new Person(5,"Harry", 27,"8-800-5");
        PersonDAO dao = new PersonDAO(new DBService());
        dao.addPerson(person1);
        dao.addPerson(person2);


        DBService dbServiceMock = Mockito.mock(DBService.class);
        Mockito.doThrow(new SQLException("error method .changeName")).when(dbServiceMock).executeQuery(Mockito.anyString());
        PersonDAO dao2 = new PersonDAO(dbServiceMock);
        boolean result = dao2.changeName(4,"Jason");
        Assertions.assertFalse(result);
    }

    /**
     * Проверяем успешный сценарий метода поиска обьекта {@Person} по номеру id;
     * С помощью метода dao.findById(7), находим обьект {@Person} под номером id равным 7,
     * ожидаем:
     *         - поле name равное "Scott",
     *         - поле age равное 25.
     */
    @Test
    public void findByIdTest() throws CustomException{
        Person person1 = new Person(6,"Oscar",22,"8-800-6");
        Person person2 = new Person(7,"Scott", 25,"8-800-7");

        PersonDAO dao = new PersonDAO(new DBService());
        dao.addPerson(person1);
        dao.addPerson(person2);

        Person tempPerson = dao.findById(7);
        Assertions.assertEquals("Scott", tempPerson.getName());
        Assertions.assertEquals(25, tempPerson.getAge());
    }

    /**
     * Проверяем неуспешный сценарий метода поиска обьекта {@Person} по номеру id;
     * При неуспешной отработке метода, ловим CustomException.
     * @throws SQLException
     * @throws CustomException
     */
    @Test
    public void findByIdTest_Fail() throws SQLException, CustomException {
        Person person1 = new Person(6,"Oscar",22,"8-800-6");
        Person person2 = new Person(7,"Scott", 25,"8-800-7");
        PersonDAO dao = new PersonDAO(new DBService());
        dao.addPerson(person1);
        dao.addPerson(person2);

        DBService dbServiceMock = Mockito.mock(DBService.class);
        Mockito.doThrow(new SQLException("error method .changeName")).when(dbServiceMock).executeQuery(Mockito.anyString());
        PersonDAO dao1 = new PersonDAO(dbServiceMock);
        Assertions.assertThrows(CustomException.class, () -> dao1.findById(7));
    }

    /**
     * Проверяем успешный сценарий считывания всех обьектов {@Person} из таблицы.
     */
    @Test
    public void readAllPerson_Success_Test() throws CustomException {

        Person person1 = new Person(8,"Lenny",29,"8-800-8");
        Person person2 = new Person(9,"Martin", 20,"8-800-9");
        PersonDAO dao = new PersonDAO(new DBService());
        dao.addPerson(person1);
        dao.addPerson(person2);

        Collection <Person> personList = dao.readAllPerson();
        Assertions.assertFalse(personList.isEmpty());
        personList.stream()
                .forEach(person -> {
                    System.out.println(person);
                });
    }

    /**
     * Проверяем неуспешный сценарий считивания всех обьектров {@Person} из таблицы.
     * При неуспешной отработке метода, ловим CustomException.
     * @throws SQLException
     * @throws CustomException
     */
    @Test
    public void readAllPerson_Fail_Test() throws SQLException{

        Person person1 = new Person(8,"Lenny",29,"8-800-8");
        Person person2 = new Person(9,"Martin", 20,"8-800-9");
        PersonDAO dao = new PersonDAO(new DBService());
        dao.addPerson(person1);
        dao.addPerson(person2);

        DBService dbServiceMock = Mockito.mock(DBService.class);
        Mockito.doThrow(new SQLException("error method .changeName")).when(dbServiceMock).executeQuery(Mockito.anyString());
        PersonDAO dao1 = new PersonDAO(dbServiceMock);
        Assertions.assertThrows(CustomException.class, () -> dao1.readAllPerson());
    }

    /**
     * Проверяем успешный сценарий удаление таблицы.
     */
    @Test
    public void dropTable_Success_Test(){
        PersonDAO dao = new PersonDAO(new DBService());
        boolean result = dao.dropTable();
        Assertions.assertTrue(result);
    }

    /**
     * Проверка неуспешного сценария удаления таблицы.
     * @throws SQLException
     */
    @Test
    public void dropTable_Fail_Test() throws SQLException {

        DBService dbServiceMock = Mockito.mock(DBService.class);
        Mockito.doThrow(new SQLException("error method .dropTable")).when(dbServiceMock).executeQuery(Mockito.anyString());
        PersonDAO dao = new PersonDAO(dbServiceMock);
        boolean result = dao.dropTable();
        Assertions.assertFalse(result);
    }
}

