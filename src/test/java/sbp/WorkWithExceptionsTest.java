package sbp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.exceptions.CustomException;
import sbp.exceptions.WorkWithExceptions;

public class WorkWithExceptionsTest {
    /**
     * в методе exceptionProcessing() ожидается подброс исключения CustomException
     */
    @Test
    public void exceptionProcessing_Test(){

        WorkWithExceptions workWithException = new WorkWithExceptions();

        Assertions.assertThrows(CustomException.class, () -> workWithException.exceptionProcessing());
    }
}



