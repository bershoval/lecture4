package sbp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sbp.CollectionDuplicate.Duplicate;

import java.util.ArrayList;
import java.util.List;

public class DuplicateTests {

       List<Integer> myListInt;
       List<String> myListStr;

    @BeforeEach
    void init() {

        myListInt = new ArrayList<>();
        myListStr = new ArrayList<>();
    }


    /**
     * Метод duplicates_firstMethod(myListInt), на вход
     * принимает коллекцию myListInt обьектов Integer (заведомо имеются дубликаты).
     * Ожидаем коллекцию listInt из элементов Integer, имеющих дубликаты.
     * После отработки метода duplicates_firstMethod(myListInt), ожидаем:
     *      - в коллекции listInt, по индексу 0 находиться обьект со значением 2;
     *      - в коллекции listInt, по индексу 1 находиться обьект со значением 8;
     *      - ожидаем размер коллекции listInt равный 2;
     *
     * Метод duplicates_firstMethod(myListStr), на вход
     * принимает коллекцию myListStr обьектов String (заведомо имеются дубликаты).
     * Ожидаем коллекцию listStr из элементов String, имеющих дубликаты.
     * После отработки метода duplicates_firstMethod(myListStr), ожидаем:
     *     - в коллекции listStr, по индексу 0 находиться обьект со значением "a";
     *     - в коллекции listStr, по индексу 1 находиться обьект со значением "b";
     *     - в коллекции listStr, по индексу 2 находиться обьект со значением "c";
     *     - ожидаем размер коллекции listStr равный 3;
     */
    @Test
    public void duplicateFirstMethod_Test(){

        Duplicate<Integer> integerDuplicate = new Duplicate<>();
        myListInt.add(2);
        myListInt.add(8);
        myListInt.add(2);
        myListInt.add(3);
        myListInt.add(1);
        myListInt.add(8);
        myListInt.add(5);
        List<Integer> listInt = integerDuplicate.duplicates_firstMethod(myListInt);
        Assertions.assertEquals(2, listInt.get(0));
        Assertions.assertEquals(8, listInt.get(1));
        Assertions.assertEquals(2,listInt.size());


        Duplicate<String> stringDuplicate = new Duplicate<>();
        myListStr.add("a");
        myListStr.add("b");
        myListStr.add("c");
        myListStr.add("b");
        myListStr.add("a");
        myListStr.add("c");
        myListStr.add("a");
        myListStr.add("f");
        List<String> listStr = stringDuplicate.duplicates_firstMethod(myListStr);
        Assertions.assertEquals("a", listStr.get(0));
        Assertions.assertEquals("b", listStr.get(1));
        Assertions.assertEquals("c", listStr.get(2));
        Assertions.assertEquals(3, listStr.size());
    }


    /**
     * Метод duplicates_secondMethod(myListInt), на вход
     * принимает коллекцию myListInt обьектов Integer (заведомо имеются дубликаты).
     * Ожидаем коллекцию listInt из элементов Integer, имеющих дубликаты.
     * После отработки метода duplicates_secondMethod(myListInt), ожидаем:
     *      - в коллекции listInt, по индексу 0 находиться обьект со значением 1;
     *      - в коллекции listInt, по индексу 1 находиться обьект со значением 2;
     *      - ожидаем размер коллекции listInt равный 2;
     *
     * Метод duplicates_secondMethod(myListStr) на вход,
     * принимает коллекцию myListStr обьектов String (заведомо имеются дубликаты).
     * Ожидаем коллекцию listStr из элементов String, имеющих дубликаты.
     * После отработки метода duplicates_secondMethod(myListStr), ожидаем:
     *     - в коллекции listStr, по индексу 0 находиться обьект со значением "a";
     *     - в коллекции listStr, по индексу 1 находиться обьект со значением "b";
     *     - в коллекции listStr, по индексу 2 находиться обьект со значением "c";
     *     - в коллекции listStr, по индексу 3 находиться обьект со значением "f";
     *     - ожидаем размер коллекции listStr равный 4;
     */
    @Test
    public void duplicateSecondMethod_Test(){

        Duplicate <Integer> integerDuplicate = new Duplicate<>();
        myListInt.add(2);
        myListInt.add(1);
        myListInt.add(2);
        myListInt.add(3);
        myListInt.add(1);
        List<Integer> listInt = integerDuplicate.duplicates_secondMethod(myListInt);
        Assertions.assertEquals(1, listInt.get(0));
        Assertions.assertEquals(2, listInt.get(1));
        Assertions.assertEquals(2, listInt.size());


        Duplicate <String> stringDuplicate = new Duplicate<>();
        myListStr.add("a");
        myListStr.add("b");
        myListStr.add("c");
        myListStr.add("b");
        myListStr.add("a");
        myListStr.add("f");
        myListStr.add("c");
        myListStr.add("s");
        myListStr.add("f");
        List<String> listStr = stringDuplicate.duplicates_secondMethod(myListStr);
        Assertions.assertEquals("a", listStr.get(0));
        Assertions.assertEquals("b", listStr.get(1));
        Assertions.assertEquals("c", listStr.get(2));
        Assertions.assertEquals("f", listStr.get(3));
        Assertions.assertEquals(4, listStr.size());
    }


    /**
     * Метод duplicates_thirdMethod(myListInt), на вход
     * принимает коллекцию myListInt обьектов Integer (заведомо имеются дубликаты).
     * Ожидаем коллекцию listInt из элементов Integer, имеющих дубликаты.
     * После отработки метода duplicates_thirdMethod(myListInt), ожидаем:
     *      - в коллекции listInt, по индексу 0 находиться обьект со значением 5;
     *      - в коллекции listInt, по индексу 1 находиться обьект со значением 7;
     *      - в коллекции listInt, по индексу 2 находиться обьект со значением 4;
     *      - ожидаем размер коллекции listInt равный 3;
     *
     * Метод duplicates_thirdMethod(myListStr) на вход,
     * принимает коллекцию myListStr обьектов String (заведомо имеются дубликаты).
     * Ожидаем коллекцию listStr из элементов String, имеющих дубликаты.
     * После отработки метода duplicates_thirdMethod(myListStr), ожидаем:
     *     - в коллекции listStr, по индексу 0 находиться обьект со значением "Alex";
     *     - в коллекции listStr, по индексу 1 находиться обьект со значением "Sten";
     *     - ожидаем размер коллекции listStr равный 2;
     */
    @Test
    public void duplicateThirdMethod_Test(){

        Duplicate <Integer> integerDuplicate = new Duplicate<>();
        myListInt.add(5);
        myListInt.add(7);
        myListInt.add(4);
        myListInt.add(5);
        myListInt.add(7);
        myListInt.add(2);
        myListInt.add(4);
        myListInt.add(8);
        List<Integer> listInt =  integerDuplicate.duplicates_thirdMethod(myListInt);
        Assertions.assertEquals(5, listInt.get(0));
        Assertions.assertEquals(7,listInt.get(1));
        Assertions.assertEquals(4,listInt.get(2));
        Assertions.assertEquals(3,listInt.size());


        Duplicate <String> stringDuplicate = new Duplicate<>();
        myListStr.add("Alex");
        myListStr.add("Max");
        myListStr.add("Sten");
        myListStr.add("Anton");
        myListStr.add("Alex");
        myListStr.add("Sten");
        List<String> listStr =  stringDuplicate.duplicates_thirdMethod(myListStr);
        Assertions.assertEquals("Alex",listStr.get(0));
        Assertions.assertEquals("Sten",listStr.get(1));
        Assertions.assertEquals(2,listStr.size());
    }
}
