package sbp;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sbp.io.MyIOExample;
import sbp.io.MyNIO2Example;

import java.io.File;

public class MyNIO2ExampleTest {

    MyNIO2Example myNIO2Example;

    public File file;
    public File copyFile;

    String sourceFileName = "MyFile";
    String nonExistentFileName = "nonExistent";
    String destinationFileName = "TestFile";

    /**
     * Иницилизация экземпляра класса {@link MyIOExample}
     * и обьекта {@link java.io.File}
     */

    @BeforeEach
    public void init() {
        myNIO2Example = new MyNIO2Example();
        file = new File(sourceFileName);
    }

    /**
     * Удаление обьекта {@link MyIOExample} destinationFileName
     * используемый при копировании
     */
    @AfterEach
    void deleteFile() {
        copyFile = new File(destinationFileName);
        copyFile.delete();
    }

    /**
     * Ожидается true в результате проверки на существование файла в методе workWithFileTest
     */
    @Test
    public void workWithFileTest(){
        boolean result = myNIO2Example.workWithFile(sourceFileName);
        Assertions.assertEquals(true,result);
    }

    /**
     * Ожидается true в результате проверки на существования файла в методе copyFile
     */
    @Test
    public void copyFileTest_True(){
        boolean result = myNIO2Example.copyFile(sourceFileName,destinationFileName);
        Assertions.assertEquals(true,result);
    }

    /**
     * Ожидается false в результате проверки на существования файла в методе copyFile
     */
    @Test
    public void copyFileTest_False(){
        boolean result = myNIO2Example.copyFile(nonExistentFileName,destinationFileName);
        Assertions.assertEquals(false,result);
    }

    /**
     * Ожидается true в результате проверки на существования файла в методе copyBufferedFile
     */
    @Test
    public void copyBufferFileTest_True(){
        boolean result = myNIO2Example.copyBufferedFile(sourceFileName,destinationFileName);
        Assertions.assertEquals(true,result);
    }

    /**
     * Ожидается false в результате проверки на существования файла в методе copyBufferedFile
     */
    @Test
    public void copyBufferFileTest_False(){
        boolean result = myNIO2Example.copyBufferedFile(nonExistentFileName,destinationFileName);
        Assertions.assertEquals(false,result);
    }

    /**
     * Ожидается true в результате проверки на существование файла в методе copyFileWithReaderAndWriter
     */
    @Test
    public void copyFileWithReaderAndWriterTest_True(){
        boolean result = myNIO2Example.copyFileWithReaderAndWriter(sourceFileName,destinationFileName);
        Assertions.assertEquals(true,result);
    }

    /**
     * Ожидается false в результате проверки на существования файла в методе copyFileWithReaderAndWriter
     */
    @Test
    public void copyFileWithReaderAndWriterTest_False(){
        boolean result = myNIO2Example.copyFileWithReaderAndWriter(nonExistentFileName,destinationFileName);
        Assertions.assertEquals(false,result);
    }

}
