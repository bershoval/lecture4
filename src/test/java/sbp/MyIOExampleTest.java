package sbp;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sbp.io.MyIOExample;

import java.io.File;
import java.io.IOException;

public class MyIOExampleTest  {
    MyIOExample myIOExample = new MyIOExample();

    public File file;
    public File copyFile;

    String sourceFileName = "MyFile";
    String nonExistentFileName = "nonExistent";
    String destinationFileName = "TestFile";

    /**
     * Иницилизация экземпляра класса {@link MyIOExample}
     * и обьекта {@link java.io.File}
     */

    @BeforeEach
    public void init() {
        myIOExample = new MyIOExample();
        file = new File(sourceFileName);
    }

    /**
     * Удаление обьекта {@link MyIOExample} destinationFileName
     * используемый при копировании
     */
    @AfterEach
    void deleteFile() {
        copyFile = new File(destinationFileName);
        copyFile.delete();
    }

    /**
     * Ожидаеться true в результате проверки на существование файла в методе workWithFileExistsObject_Test
     */
    @Test
    public void workWithFileExistsObject_Test()  {
        boolean result = myIOExample.workWithFile(sourceFileName);
        Assertions.assertTrue(result);
    }

    /**
     * Ожидаеться true в результате проверки на существования файла в методе copyFile
     *
     */
    @Test
    public void copyFile_Test()  {
        boolean result = myIOExample.copyFile(sourceFileName,destinationFileName);
        Assertions.assertTrue(result);
    }

    /**
     * Ожидаеться false в результате проверки на существования файла в методе copyFile
     *
     */
    @Test
    public void copyFile_Test_False() {
        boolean result = myIOExample.copyFile(nonExistentFileName,destinationFileName);
        Assertions.assertFalse(result);
    }

    /**
     * Ожидаеться true в результате проверки на существования файла в методе copyBufferedFile
     */
    @Test
    public void copyBufferedFile_Test(){
        boolean result = myIOExample.copyBufferedFile(sourceFileName,destinationFileName);
        Assertions.assertTrue(result);
    }

    /**
     * Ожидаеться false в результате проверки на существования файла в методе copyBufferedFile
     *
     */
    @Test
    public void copyBufferedFile_Test_False() {
        boolean result = myIOExample.copyBufferedFile(nonExistentFileName,destinationFileName);
        Assertions.assertFalse(result);
    }

    /**
     * Ожидаеться true в результате проверки на существование файла в методе copyFileWithReaderAndWriter
     */
    @Test
    public void copyFileWithReaderAndWriter_Test(){
        boolean result = myIOExample.copyFileWithReaderAndWriter(sourceFileName,destinationFileName);
        Assertions.assertTrue(result);
    }

    /**
     * Ожидаеться false в результате проверки на существования файла в методе copyFileWithReaderAndWriter
     *
     */
    @Test
    public void copyFileWithReaderAndWriter_False() {

        boolean result = myIOExample.copyFileWithReaderAndWriter(nonExistentFileName,destinationFileName);
        Assertions.assertFalse(result);
    }
}