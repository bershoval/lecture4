package sbp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.cmp.CustomDigitComparator;
import sbp.exceptions.CustomException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CustomDigitComparatorTests {

    /**
     * После отработки метода sort(cmpByInteger) жидается сортировка обьектов
     * коллекции list1, сначала четные числа, затем нечетные;
     * Для проверки создана коллекция list2, в которую добавленны элементы
     * в последовательности [2, 4, 6, 8, 10, 1, 3, 5, 9, 11] (сначала четные числа, затем нечетные);
     * После отработки метода assertEquals(list2,list1), ожидаеться, что последовательность
     * расположения элементов коллекции list1 будет эквивалентна расположению элементов в коллекции list2.
     */
    @Test
    public void CustomDigitComparatorIntegerTest(){

        List<Integer> list1 = new ArrayList<>();

        list1.add(1);
        list1.add(2);
        list1.add(3);
        list1.add(4);
        list1.add(5);
        list1.add(6);
        list1.add(8);
        list1.add(9);
        list1.add(10);
        list1.add(11);

        Comparator<Integer> cmpByInteger = new CustomDigitComparator();
        list1.sort(cmpByInteger);
        System.out.println("list1: " + list1);

        List<Integer> list2 = new ArrayList<>();
        list2.add(2);
        list2.add(4);
        list2.add(6);
        list2.add(8);
        list2.add(10);
        list2.add(1);
        list2.add(3);
        list2.add(5);
        list2.add(9);
        list2.add(11);

        Assertions.assertEquals(list2,list1);
        System.out.println("list2: " + list2);
    }

    /**
     * При поподании на вход метода compare(Integer lhs, Integer rhs) агумента со значением null,
     * ожидаем подброс исключения IllegalArgumentException.
     */
    @Test
    public void argumentIsNull(){

        CustomDigitComparator customDigitComparator = new CustomDigitComparator();
        Assertions.assertThrows(IllegalArgumentException.class, () -> customDigitComparator.compare(null,1));

    }
}
