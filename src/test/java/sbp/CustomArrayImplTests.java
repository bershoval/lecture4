package sbp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.collections.CustomArray;
import sbp.collections.CustomArrayImpl;
import sbp.exceptions.WorkWithExceptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;


public class CustomArrayImplTests {

    CustomArrayImpl<Integer> customArray;

    /**
     * Создание объекта тестируемого класса CustomArrayImpl
     */
    @BeforeEach
    void init() {
        customArray = new CustomArrayImpl<>();
    }

    /**
     * Тест для конструктора CustomArrayImpl(int capacity);
     * Ожидаем размер constructorInputCapacity равный 0;
     * Ожидаем capacity customArrayInCapacity равное 15;
     * После добавления 3 элементов ожидаем значеие customArrayInCapacity.size() равное 3;
     * После метода ensureCapacity(13), ожидаем capacity customArrayInCapacity равное 25
     */
    @Test
    public void constructorInputCapacity() {
        CustomArrayImpl<Integer> customArrayInCapacity  = new CustomArrayImpl<>(15);

        Assertions.assertEquals(0, customArrayInCapacity.size());
        Assertions.assertEquals(15, customArrayInCapacity.getCapacity());

        customArrayInCapacity.add(1);
        customArrayInCapacity.add(2);
        customArrayInCapacity.add(3);

        Assertions.assertEquals(3, customArrayInCapacity.size());
        customArrayInCapacity.ensureCapacity(13);
    }
    /**
     * Тест для конструктора CustomArrayImpl(Collection<T> c);
     * Ожидаем размер customArrayInCollection равный 3;
     * Ожидаем capacity customArrayInCollection равное 10;
     * Ожидаем значение в customArrayInCollection под индексом 1 равное 2;
     * После метода ensureCapacity(8), ожидаем capacity customArrayInCollection равное 20
     *
     */
    @Test
    public void constructorInputCollection() {
        List<Integer> tempList = new ArrayList<>();
        tempList.add(1);
        tempList.add(2);
        tempList.add(3);
        CustomArrayImpl<Integer> customArrayInCollection = new CustomArrayImpl<>(tempList);

        Assertions.assertEquals(3, customArrayInCollection.size());
        Assertions.assertEquals(10, customArrayInCollection.getCapacity());
        Assertions.assertEquals(2, customArrayInCollection.get(1));

        customArrayInCollection.ensureCapacity(8);
        Assertions.assertEquals(20,customArrayInCollection.getCapacity());
    }

    /**
     * Ожидаем true от метода isEmpty(), коллекция пуста.
     */
    @Test
    public void IsEmptyTest(){
        Assertions.assertTrue(customArray.isEmpty());
    }

    /**
     * После отработки метода size(), ожидаем возарат размера коллекции равного 2.
     */
    @Test
    public void sizeTest(){
        customArray.add(1);
        customArray.add(2);
        Assertions.assertEquals(2, customArray.size());
    }

    /**
     * После отработки метода add(10), ожидаем возарат размера коллекции равного 1.
     * Проверяем, что по индексу 0, присудствует дабавленое нами число 10.
     */
    @Test
    public void AddTest(){
        customArray.add(10);

        Assertions.assertEquals(1, customArray.size());
        Assertions.assertEquals(10, customArray.get(0));
    }

    /**
     * Ожидаем возврат true, после отработки метода addAll(T[] items).
     * Ожидаем размер customArray равный 4.
     *
     */
    @Test
    public void AddAllArraysTest(){

        customArray.add(1);
        customArray.add(2);

        Integer[] newArray = {3,4};

        Assertions.assertTrue(customArray.addAll((newArray)));
        Assertions.assertEquals(4,customArray.size());
    }

    /**
     * Ожидаеем возврат true, после отработки метода addAll(Collection <T> items).
     * Ожидаеться размер коллекции customArray равный 12;
     * Ожидаем capacity customArray равное 20;
     */
    @Test
    public void AddAllCollectionTest(){

        customArray.add(1);
        customArray.add(2);
        customArray.add(3);

        Assertions.assertEquals(10, customArray.getCapacity());
        List<Integer> newCollection = new ArrayList<>();
        newCollection.add(4);
        newCollection.add(5);
        newCollection.add(6);
        newCollection.add(7);
        newCollection.add(8);
        newCollection.add(9);
        newCollection.add(10);
        newCollection.add(11);
        newCollection.add(12);

        Assertions.assertTrue(customArray.addAll(newCollection));
        Assertions.assertEquals(12,customArray.size());
        Assertions.assertEquals(20, customArray.getCapacity());
    }

    /**
     * Ожидаеем возврат true, после отработки метода addAll(int index, Object[] items).
     * После отработки метода addAll() в коллекции customArray ожидаем:
     *  - под индексом 5 значение 7,
     *  - под индексом 6 значение 7,
     *  - под индексом 7 значение 7.
     *  После отработки метода size(), ожидаем размер коллекции customArray равный 13
     *  Ожидаем capacity customArray равное 20
     */
    @Test
    public void AddAllIndexTest(){

        customArray.add(1);
        customArray.add(2);
        customArray.add(3);
        customArray.add(4);
        customArray.add(5);
        customArray.add(6);
        customArray.add(7);
        customArray.add(8);
        customArray.add(9);
        customArray.add(10);

        Integer[] newArray = {7,7,7};

        Assertions.assertTrue(customArray.addAll(5, newArray));
        Assertions.assertEquals(7, customArray.get(5));
        Assertions.assertEquals(7, customArray.get(6));
        Assertions.assertEquals(7, customArray.get(7));
        Assertions.assertEquals(13, customArray.size());
        Assertions.assertEquals(20, customArray.getCapacity());
    }

    /**
     * Ожидаем значение 2 по индексом 1 коллексии customArray.
     */
    @Test
    public void GetTest(){
        customArray.add(1);
        customArray.add(2);
        customArray.add(3);

        Assertions.assertEquals(2, customArray.get(1));
        Assertions.assertEquals(3, customArray.get(2));
    }

    /**
     * Ожидаем возврат значения 2 под индексом 1, после завершения работы метода set(1,4).
     * Проверяем, измененное заначение под индексом 1, ожидаем значение 4.
     */
    @Test
    public void SetTest(){
        customArray.add(1);
        customArray.add(2);
        customArray.add(3);

        Assertions.assertEquals(2, customArray.set(1,4));
        Assertions.assertEquals(4, customArray.get(1));
    }

    /**
     * После отработки метода remove(int index) в коллекции customArray ожидаем:
     * под индексом 0 значение 1,
     * под индексом 1 значение 3,
     * под индексом 2 значение 4.
     * После отработки метода size(), ожидаем размер коллекции customArray равный 3
     */
    @Test
    public void RemoveTest(){
        customArray.add(1);
        customArray.add(2);
        customArray.add(3);
        customArray.add(4);

        customArray.remove(1);
        Assertions.assertEquals(1, customArray.get(0));
        Assertions.assertEquals(3, customArray.get(1));
        Assertions.assertEquals(4, customArray.get(2));
        Assertions.assertEquals(3, customArray.size());
    }

    /**
     * Проверяем уменьшение размера коллекции customArray, после применения метода remove(T item)
     * ожидаем значение размера коллекции равное 3.
     * Ожидаем false при проверке коллекции customArray на наличие удаленного элемента otherValue = 2
     * с помощью метода contains(T t).
     */
    @Test
    public void Remove_T_Test(){
        customArray.add(1);
        customArray.add(2);
        customArray.add(3);
        customArray.add(4);

        Integer otherValue = 2;
        Assertions.assertTrue(customArray.remove(otherValue));
        Assertions.assertEquals(3,customArray.size());
        Assertions.assertFalse(customArray.contains(otherValue));
    }

    /**
     * Ожидаем true при проверке коллекци customArray на наличие элемента otherValue = 3
     * с помощью метода contains(T t)
     */
    @Test
    public void ContainsTest(){
        customArray.add(1);
        customArray.add(2);
        customArray.add(3);
        customArray.add(4);

        Integer otherValue = 3;
        Assertions.assertTrue(customArray.contains(otherValue));
    }

    /**
     * Проверка на наличие элемента со значением 3 под индексом 2, с помощью метода indexOf(T item)
     */
    @Test
    public void IndexOfTest(){
        customArray.add(1);
        customArray.add(2);
        customArray.add(3);
        customArray.add(4);

        Integer otherValue = 3;
        Assertions.assertEquals(2, customArray.indexOf(otherValue));
    }

    /**
     * Проверка расширения Capacity при использовании метода ensureCapacity(int newElementsCount)
     * при установке значения newElementsCount = 7, ожидаем значения capacity равное 20.
     */
    @Test
    public void EnsureCapacityTest(){
        customArray.add(1);
        customArray.add(2);
        customArray.add(3);
        customArray.add(4);

        customArray.ensureCapacity(6);
        int getCapacityOld = customArray.getCapacity();
        Assertions.assertEquals(10, getCapacityOld);

        customArray.ensureCapacity(7);
        int getCapacityNew = customArray.getCapacity();
        Assertions.assertEquals(20, getCapacityNew);
    }

    /**
     * с помощью метода getCapacity() получаем текущее capacity
     * с помощью assertEquals() сравниваем ее с дефолтным значением 10, определенным при создании customArray
     */
    @Test
    public void GetCapacityTest(){
        Assertions.assertEquals(10, customArray.getCapacity());
    }

    /**
     * С помощью метода reverse(), реверсируем все элементы коллекции customArray
     * С помощью assertEquals(), сравниваем расположение элементов коллкции customArray относительно индексов
     */
    @Test
    public void ReverseTest(){
        customArray.add(1);
        customArray.add(2);
        customArray.add(3);
        customArray.add(4);

        customArray.reverse();
        Assertions.assertEquals(4, customArray.get(0));
        Assertions.assertEquals(3, customArray.get(1));
        Assertions.assertEquals(2, customArray.get(2));
        Assertions.assertEquals(1, customArray.get(3));
        Assertions.assertEquals(4, customArray.size());
    }

    /**
     * Провека создания масива обьектов Object [] obj, с помощью метода toString()
     * Ожидаем значение 1 в массиве obj по идексом 0;
     * Ожидаем значение 2 в массиве obj по идексом 1;
     */
    @Test
    public void ToArrayTest(){
        customArray.add(1);
        customArray.add(2);
        Object [] obj =  customArray.toArray();
        Assertions.assertEquals(1,obj[0]);
        Assertions.assertEquals(2,obj[1]);
    }
}